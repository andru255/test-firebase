//
//  RemoteConfigInterface.swift
//  test-firebase
//
//  Created by mobile on 29/05/17.
//  Copyright © 2017 mobile. All rights reserved.
//

import Foundation
import Firebase

struct remoteConfigKeys {
    let data: String = "data";
    let country: String = "countries";
}

class RemoteConfigAdapter {
    
    static let shared: RemoteConfigAdapter = RemoteConfigAdapter();
    let remoteConfig: RemoteConfig!;
    let KeyNames = remoteConfigKeys();
    
    //internal let strDataContent = "{\"color\": \"red\", \"available\": \"modo offline\"}";
    internal var dictionaryDataContent: [String: String]? = [:];
    internal var dictionaryCountryContent: [[String: Any?]]? = [];
    
    /*internal let webConfigContent = [
        //"data": "{\"color\": \"red\"}"
        "data": [ "color": [] ]
    ]; */

    private init() {
        /*START: remoteConfig*/
        remoteConfig = RemoteConfig.remoteConfig();
        /*END  : remoteConfig*/
        
        /*START: enable dev mode*/
        let remoteConfigSettings = RemoteConfigSettings(developerModeEnabled: true);
        remoteConfig.configSettings = remoteConfigSettings!;
        /*END  : enable dev mode*/
        
        //Required a NSObject or data parsed to NSString or others too
        /*remoteConfig.setDefaults([
            self.dataConfigKey: strDataContent as NSString
        ]);*/
        remoteConfig.setDefaults(fromPlist: "FirebaseRemoteConfig-DefaultValues");
        
        remoteConfig.activateFetched();
        
        let dataFromRemoteConfig      = remoteConfig.configValue(forKey: KeyNames.data).stringValue ?? "{}"
        let countriesFromRemoteConfig = remoteConfig.configValue(forKey: KeyNames.country).stringValue ?? "[]"

        print("dataFromRemoteConfig------>\(String(describing: dataFromRemoteConfig))");
        print("countriesFromRemoteConfig------>\(String(describing: countriesFromRemoteConfig))");
        
        self.dictionaryDataContent    = self._convertStringToDictionary(text: dataFromRemoteConfig);
        self.dictionaryCountryContent = self._convertStringToArrayDictionary(text: countriesFromRemoteConfig);
        
        print("self.dictionaryCountryContent------>\(String(describing: self.dictionaryCountryContent))");

        
        self._fetchConfig();
    }
    
    private func _fetchConfig(){
        
        /*START: only for dev mode*/
        var expirationDuration = 3600;
        if remoteConfig.configSettings.isDeveloperModeEnabled {
            expirationDuration = 0;
        }
        /*END  : only for dev mode*/
        
        remoteConfig.fetch(withExpirationDuration: TimeInterval(expirationDuration)) { (status, error) -> Void in
            print ("status: \(String(describing: status))");
            if status == .success {
                print ("config fetched!");
                //self.remoteConfig.activateFetched();
            } else {
                print ("sorry config not fetched");
                print ("Error \(error!.localizedDescription)");
            }
            //self._displayData();
        }
    }
    
    private func _convertStringToDictionary(text: String) -> [String: String]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: String]
            } catch {
                print("ERROR _convertToDictionary \(error)");
            }
        }
        return nil;
    }
    
    private func _convertStringToArrayDictionary(text: String) -> [[String: Any?]]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [[String: Any?]]
            } catch {
                print("ERROR _convertToDictionary \(error)");
            }
        }
        return nil;
    }
    
}
