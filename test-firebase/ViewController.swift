//
//  ViewController.swift
//  test-firebase
//
//  Created by mobile on 24/05/17.
//  Copyright © 2017 mobile. All rights reserved.
//

import UIKit

class ViewController: UIViewController,
   UIPickerViewDataSource,
   UIPickerViewDelegate {

    var dictionaryCountryContent: [[String: Any?]] = [];
    var dataSelected: [String: Any?] = ["id": "00", "name": "please Select one"];
    
    // MARK: outlets
    @IBOutlet weak var lblColor: UILabel!;
    @IBOutlet weak var lblProgress: UILabel!;
    @IBOutlet weak var countryPicker: UIPickerView!;
    @IBOutlet weak var lblCountry: UILabel!;
    
    // MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self._onPreviewDisplayData();
        
        // getting the data from the adapter
        let dictionaryDataContent = RemoteConfigAdapter.shared.dictionaryDataContent;
        
        // getting the countries from the adapter
        let dataCountries = RemoteConfigAdapter.shared.dictionaryCountryContent!;
        self.dictionaryCountryContent = dataCountries.sorted {
            self._parseToInt($0["order"] ?? "") < self._parseToInt($1["order"] ?? "")
        };
        
        
        self._displayData(dictionaryData: dictionaryDataContent);
        
        //offline: my config is: Optional(["color": "red"])
        //online :  my config is: Optional(["version": "true", "color": "green"])
        print(" MY CONFIG is: \(String(describing: dictionaryDataContent))");
        print(" MY COUNTRIES IN CONFIG is: \(String(describing: self.dictionaryCountryContent))");
    
        // delegation of picker view
        self.countryPicker.dataSource = self;
        self.countryPicker.delegate = self;
    }
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    private func _onPreviewDisplayData() {
        lblProgress.text = "cargando...";
    }
    
    private func _displayData( dictionaryData:[String: String]? ){
        //String! width "!" unwraps the optional value
        
        let configColor: String     = dictionaryData!["color"] as String!;
        let configAvailable: String = dictionaryData!["available"] as String!;
        
        //Showing data...
        lblColor.text = configColor;
        lblProgress.text = configAvailable;
    }
    
    private func _parseToInt(_ anyValue: Any? ) -> Int{
        var result: Int = 0;
        if let stringValue =  anyValue as? String {
            result = Int(stringValue) ?? 0;
        }
        return result;
    };

    /*MARK: - start required for UIPickerViewDataSource*/
    /*total of pickers*/
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1;
    }
    
    /*total of options in picker(s)*/
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.dictionaryCountryContent.count;
    }
    
    /*return the value of every option component*/
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        let rowData: [String: Any?] = self.dictionaryCountryContent[row];
        return rowData["name"] as! String?;
    }
    
    /*listen events on picker view*/
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let RowSelected: Int = countryPicker.selectedRow(inComponent: 0);
        self.dataSelected = self.dictionaryCountryContent[RowSelected];
        print("component -> \(String(describing: dataSelected))");
        self.updateLabel();
    }
    /*MARK: - end required for UIPickerViewDataSource*/
    
    func updateLabel() -> Void {
        let dataToShow: [String] =  [
            "id: \(self.dataSelected["id"] as! String)",
            " name: \(self.dataSelected["name"] as! String)",
            " order: \(self.dataSelected["order"] as! String)"
        ];
        lblCountry.text = dataToShow.joined(separator: ",");
    }
    
    

}

